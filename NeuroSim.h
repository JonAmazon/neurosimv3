#ifndef _H_NEUROSIM
#define _H_NEUROSIM

#include <ctime>
#include "PicoGlobal.h"
#include "StatRand.h"

class NonLinearFilter
{
public:
    enum FilterType {PWL,EXP,SQRT,DSC};

    virtual void initialize(float A, float B, float C, float D) = 0;
    virtual float evaluate(float u) = 0;
    virtual float evaluateDerivative(float u) = 0;
    virtual float getMidPoint() = 0;
private:
};

class NonLinearFilterPWL : public NonLinearFilter
{
public:
	void initialize(float A, float B, float C, float D)
	{
		threshold = A;
		saturation = B;
		steepness = C;
	}

	float evaluate(float u)
	{
		float returnVal;

		if(u < threshold)
		{
			returnVal = 0.0;
		}
		else if(u < saturation)
		{
			returnVal = steepness*(u - threshold);
		}
		else
		{
			returnVal = steepness*(saturation-threshold);
		}

		return returnVal;
	}
	float evaluateDerivative(float u)
	{
		float returnVal;

		if(u < threshold)
		{
			returnVal = 0.0;
		}
		else if(u < saturation)
		{
			returnVal = steepness;
		}
		else
		{
			returnVal = 0.0;
		}

		return returnVal;
	}

	float getMidPoint()
	{
		return 0.5*(saturation+threshold);
	}

private:
	float threshold;
	float saturation;
	float steepness;
};

class NonLinearFilterExp : public NonLinearFilter
{
public:
	void initialize(float A, float B, float C, float D)
	{
		midPoint = A;
		satlevel = B;
		steepness = C;
	}

	float evaluate(float u)
	{
		float returnVal;

		returnVal = satlevel/(1 + expf(-4.0*steepness/satlevel*(u - midPoint)));

		return returnVal;
	}

	float evaluateDerivative(float u)
	{
		float returnVal;
		float exponentialVal,denomVal;

		exponentialVal = expf(-4.0*steepness/satlevel*(u - midPoint));
		denomVal = (1 + exponentialVal)*(1 + exponentialVal);
		returnVal = 4*steepness*exponentialVal/denomVal;

		return returnVal;
	}

	float getMidPoint()
	{
		return midPoint;
	}

private:
	float midPoint;
	float satlevel;
	float steepness;
};

class NonLinearFilterSqrt : public NonLinearFilter
{
public:
	void initialize(float A, float B, float C, float D)
	{
		threshold = A;
		multiplier = B;
	}

	float evaluate(float u)
	{
		float arg,returnVal;
        arg = u-threshold;

        if(arg < 0.0)
        {
            returnVal = 0.0;
        }
        else
        {
            returnVal = multiplier*sqrtf(arg);
        }

		return returnVal;
	}

	float evaluateDerivative(float u)
	{
		float arg,returnVal;
        arg = u-threshold;

        if(arg < 0.0)
        {
            returnVal = 0.0;
        }
        else
        {
            returnVal = 0.5*multiplier/sqrtf(arg);
        }

		return returnVal;
	}

	float getMidPoint()
	{
		return threshold;
	}

private:
	float threshold;
	float multiplier;
};

class NonLinearFilterDsc : public NonLinearFilter
{
public:
	void initialize(float A, float B, float C, float D)
	{
		threshold = A;
        vthreshold = B;
		multiplier = C;
	}

	float evaluate(float u)
	{
		float arg,returnVal;
        arg = u-vthreshold;

        if(arg < 0.0)
        {
            returnVal = 0.0;
        }
        else
        {
            returnVal = multiplier*sqrtf(arg);
        }

        if(u < threshold)
        {
            returnVal = 0;
        }
        else{}

		return returnVal;
	}

	float evaluateDerivative(float u)
	{
		float arg,returnVal;
        arg = u-threshold;

        if(arg < 0.0)
        {
            returnVal = 0.0;
        }
        else
        {
            returnVal = 0.5*multiplier/sqrtf(arg);
        }

        if(u < threshold)
        {
            returnVal = 0;
        }
        else{}

		return returnVal;
	}

	float getMidPoint()
	{
		return threshold;
	}

private:
	float threshold;
    float vthreshold;
	float multiplier;
};

class FixedPointAnalyzer
{
public:
    void initialize(int numberOfSamplesIn,int dimIn,float toleranceIn);
    void findFixedPoints(float** samples);
    void release();

    int getNumberOfFixedPoints(){return numberOfFixedPoints;}
    int getSampleToFixedPoint(int s){return sampleToFixedPoint[s];}
    float *getFixedPoint(int f){return fixedPoints[f];}

private:
    int numberOfFixedPoints,numberOfSamples,dim;
    float tolerance;
    float **fixedPoints;
    int *sampleToFixedPoint;

    float distanceL2(float *v1, float *v2)
    {
        float r = 0.0;
        for(int i = 0; i < dim; ++i)
        {
            r += (v1[i]-v2[i])*(v1[i]-v2[i]);
        }
        return r;
    }
};

class NeuroSim
{
public:
	enum TrajectoryStateEnum
	{
		TS_SILENT,
		TS_LOUD,
		TS_KREGION,
		TS_TREGION,
		TS_DEFAULT
	};

	void initialize(const char *paramf);
	void update(int numberOfIterations);
    void updateWeights(float* image);
	void release();

	int getNumberOfTrajectories(){return numberOfTrajectories;}
	int getNumberOfNeurons(){return numberOfNeurons;}
    int getSeed(){return seed;}

	TrajectoryStateEnum getTrajectoryState(int t){return trajectoryStates[t];}

    float getSimulationTime(){return updateCounter*timeStep;}
    int getSimulationUpdateCounter(){return updateCounter;}
    float getInitialConditionsExc(int t, int n){return initialConditionsExc[t][n];}
	float getActivityExc(int t, int n){return activityExc[t][n];}
	float getActivityInh(int t, int n){return activityInh[t][n];}
	float getExternalDrive(int t,int n){return externalDrive[t][n];}
    float getWeightExc(int post,int pre){return weightEE[post][pre];}
    float getWeightInh(int post,int pre){return weightEI[post][pre];}
    FixedPointAnalyzer* getFixedPointAnalyzer(){return &fixedPointAnalyzer;}
    NonLinearFilter* getNonLinearResponseExc(){return responseExc;}
    NonLinearFilter* getNonLinearResponseInh(){return responseInh;}

    void setExternalDrive(int t, int n, float r){externalDrive[t][n] = r;}
    void setExternalDriveAll(float r)
    {
        for(int t = 0; t < numberOfTrajectories; ++t)
        {
            for(int n = 0; n < numberOfNeurons; ++n)
            {
                externalDrive[t][n] = r;
            }
        }
    }
	void setActivityExc(int t, int n, float f){activityExc[t][n] = f;}
	void setActivityInh(int t, int n, float f){activityInh[t][n] = f;}

    void setupNewSimulationRandom();
    float setupNewSimulationNoisySynapses(); //returns noise STD
    void setupNewSimulationPerturb(float *dir, float mag);
	void resetTrajectories(){resetActvityRandom();}
    void resetExternalDriveRandom(float a, float b);
    void findFixedPoints(){fixedPointAnalyzer.findFixedPoints(activityExc);}

private:
	int numberOfNeurons,numberOfTrajectories;
	int updateCounter;
	float tauExc,tauInh,timeStep;
    float externalDriveMin,externalDriveMax;

    int seed;
	stdRan* random;

	float *k1Exc,*k2Exc,*k3Exc,*k4Exc;
	float *k1Inh,*k2Inh,*k3Inh,*k4Inh;

	float **externalDrive;
    float **initialConditionsExc;
	float **activityExc; //activity[trajectory index][neuron index]
	float **inputExc;
	float **inputInh;
	float **activityInh;
	float **weightEE; //syanptic weights[post][pre]
	float **weightEI;
    float **weightIE;
    float **weightII;

	TrajectoryStateEnum* trajectoryStates;

	NonLinearFilter* responseExc;
	NonLinearFilter* responseInh;

    FixedPointAnalyzer fixedPointAnalyzer;

	
    void resetExternalDriveZero();
	
	void classifyTrajectoryStates();
	void resetWeightsStandard(float lambdaE, float lambdaI);
	void resetActvityRandom();
	void somaticFilterModelRK4Step();
    void synapticFilterModelRK4Step();

    void loadMatrixFromFile(float** M,float mult,const char *file);
};

#endif