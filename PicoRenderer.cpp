#include "PicoRenderer.h"

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

void hueColor(float& R, float& G, float& B, double p, double sat)
{
	if(p <= 1.0/11.0){R = 1; G = 11.0/2.0*(p); B = 0;}
	else if(p <= 2.0/11.0){R = 1; G = 11.0/2.0*(p); B = 0;}
	else if(p <= 3.0/11.0){R = -11.0/2.0*(p-4.0/11.0); G = 1; B = 0;}
	else if(p <= 4.0/11.0){R = -11.0/2.0*(p-4.0/11.0); G = 1; B = 0;}
	else if(p <= 5.0/11.0){R = 0; G = 1; B = 11.0/2.0*(p-4.0/11.0);}
	else if(p <= 6.0/11.0){R = 0; G = 1; B = 11.0/2.0*(p-4.0/11.0);}
	else if(p <= 7.0/11.0){R = 0; G = -11.0/2.0*(p-8.0/11.0); B = 1;}
	else if(p <= 8.0/11.0){R = 0; G = -11.0/2.0*(p-8.0/11.0); B = 1;}
	else if(p <= 9.0/11.0){R = 11.0/2.0*(p-8.0/11.0); G = 0; B = 1;}
	else if(p <= 10.0/11.0){R = 11.0/2.0*(p-8.0/11.0); G = 0; B = 1;}
	else{R = 1; G = 0; B = 1;}
	R = sat*R+(1-sat); G = sat*G+(1-sat); B = sat*B+(1-sat);
}

void IndexColord(float& R, float& G, float& B, int idx, float sat)
{
	//find what RGB values correspond to idx
    R = 0.5; G = 0.5; B = 0.5;
    if((idx+1) == 1){R = 1.0; G = 0.0; B = 0.0;}
    if((idx+1) == 2){R = 1.0; G = 1.0; B = 0.0;}
    if((idx+1) == 3){R = 0.0; G = 1.0; B = 0.0;}
    if((idx+1) == 4){R = 0.0; G = 1.0; B = 1.0;}
    if((idx+1) == 5){R = 0.0; G = 0.0; B = 1.0;}
    if((idx+1) == 6){R = 1.0; G = 0.0; B = 1.0;}
    if((idx+1) == 7){R = 1.0; G = 1.0; B = 1.0;}
    
	//set saturation and RGB levels
	R = sat*R+(1-sat); G = sat*G+(1-sat); B = sat*B+(1-sat);
}

void drawSphere()
{
	int n = 16;
	float pi =  3.14159265358979323846264338327950288419716939937510;

	//draw body
	glBegin(GL_QUADS);
	for(int i = 0; i < 2*n; ++i)
	{
		for(int j = 1; j < n-1; ++j)
		{
			glNormal3f(sinf(j*pi/n)*sinf(i*pi/n),
					   sinf(j*pi/n)*cosf(i*pi/n),
					   cosf(j*pi/n));
			glVertex3f(sinf(j*pi/n)*sinf(i*pi/n),
					   sinf(j*pi/n)*cosf(i*pi/n),
					   cosf(j*pi/n));
			glVertex3f(sinf(j*pi/n)*sinf((i+1)*pi/n),
					   sinf(j*pi/n)*cosf((i+1)*pi/n),
					   cosf(j*pi/n));
			glVertex3f(sinf((j+1)*pi/n)*sinf((i+1)*pi/n),
					   sinf((j+1)*pi/n)*cosf((i+1)*pi/n),
					   cosf((j+1)*pi/n));
			glVertex3f(sinf((j+1)*pi/n)*sinf(i*pi/n),
					   sinf((j+1)*pi/n)*cosf(i*pi/n),
					   cosf((j+1)*pi/n));
		}
	}
	glEnd();

	//draw caps
	
	glBegin(GL_TRIANGLES);
	glColor3f(1.0,1.0,1.0);
	for(int i = 0; i < 2*n; ++i)
	{
		glNormal3f(0.0,0.0,1.0);
		glVertex3f(0.0,0.0,1.0);
		glVertex3f(sinf((1)*pi/n)*sinf((i+1)*pi/n),
					   sinf((1)*pi/n)*cosf((i+1)*pi/n),
					   cosf((1)*pi/n));
		glVertex3f(sinf((1)*pi/n)*sinf(i*pi/n),
				   sinf((1)*pi/n)*cosf(i*pi/n),
				   cosf((1)*pi/n));
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(1.0,1.0,1.0);
	for(int i = 0; i < 2*n; ++i)
	{
		glNormal3f(0.0,0.0,-1.0);
		glVertex3f(0.0,0.0,-1.0);
		glVertex3f(-sinf((1)*pi/n)*sinf((i+1)*pi/n),
				   -sinf((1)*pi/n)*cosf((i+1)*pi/n),
				   -cosf((1)*pi/n));
		glVertex3f(-sinf((1)*pi/n)*sinf(i*pi/n),
				   -sinf((1)*pi/n)*cosf(i*pi/n),
				   -cosf((1)*pi/n));
	}
	glEnd();
}

void drawBox()
{
	glBegin(GL_QUADS);
	glNormal3f(0.0,-1.0,0.0);
	glVertex3f(-1.0,-1.0,-1.0);
	glVertex3f(1.0,-1.0,-1.0);
	glVertex3f(1.0,-1.0,1.0);
	glVertex3f(-1.0,-1.0,1.0);

	glNormal3f(0.0,1.0,0.0);
	glVertex3f(-1.0,1.0,-1.0);
	glVertex3f(1.0,1.0,-1.0);
	glVertex3f(1.0,1.0,1.0);
	glVertex3f(-1.0,1.0,1.0);
	glEnd();
}

void drawLineCube()
{
	
	glBegin(GL_LINES);
	glColor3f(1.0,1.0,1.0);

	glVertex3f(0.0,0.0,0.0);
	glVertex3f(1.0,0.0,0.0);

	glVertex3f(1.0,0.0,0.0);
	glVertex3f(1.0,1.0,0.0);

	glVertex3f(1.0,1.0,0.0);
	glVertex3f(0.0,1.0,0.0);

	glVertex3f(0.0,1.0,0.0);
	glVertex3f(0.0,0.0,0.0);

	glVertex3f(0.0,0.0,1.0);
	glVertex3f(1.0,0.0,1.0);

	glVertex3f(1.0,0.0,1.0);
	glVertex3f(1.0,1.0,1.0);

	glVertex3f(1.0,1.0,1.0);
	glVertex3f(0.0,1.0,1.0);

	glVertex3f(0.0,1.0,1.0);
	glVertex3f(0.0,0.0,1.0);

	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,0.0,1.0);

	glVertex3f(1.0,0.0,0.0);
	glVertex3f(1.0,0.0,1.0);

	glVertex3f(1.0,1.0,0.0);
	glVertex3f(1.0,1.0,1.0);

	glVertex3f(0.0,1.0,0.0);
	glVertex3f(0.0,1.0,1.0);

	glColor3f(1.0,1.0,1.0);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(1.0,1.0,1.0);

	glEnd();
}

void drawLineAxes()
{
    glLineWidth(4.0);
	glBegin(GL_LINES);
	glColor3f(1.0,0.7,0.7);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(1.0,0.0,0.0);

    glColor3f(0.7,1.0,0.7);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,1.0,0.0);

    glColor3f(0.7,0.7,1.0);
	glVertex3f(0.0,0.0,0.0);
	glVertex3f(0.0,0.0,1.0);

	glEnd();
}

void drawActivitySpacePump(NeuroSim* nsim)
{
	int ntraj = nsim->getNumberOfTrajectories();
	int nneur = nsim->getNumberOfNeurons();
	float R,G,B;

    glPushMatrix();
    glScalef(0.3,0.3,0.3);
    drawLineAxes();
    glPopMatrix();

	glPointSize(6.0);
	glBegin(GL_POINTS);
	for(int i = 0; i < ntraj; ++i)
	{
		hueColor(R,G,B,(nsim->getExternalDrive(i,0)),1.0);
		glColor3f(R,G,B);
		glVertex3f(nsim->getActivityExc(i,0),nsim->getActivityExc(i,1),nsim->getActivityExc(i,2));
	}
	glEnd();
	glPointSize(1.0);
}

void drawActivitySpaceRegion(NeuroSim* nsim)
{
	int ntraj = nsim->getNumberOfTrajectories();
	int nneur = nsim->getNumberOfNeurons();
	NeuroSim::TrajectoryStateEnum trajState;
	float R,G,B;

	glPushMatrix();
    glScalef(0.3,0.3,0.3);
    drawLineAxes();
    glPopMatrix();

	glPointSize(6.0);
	glBegin(GL_POINTS);
	for(int i = 0; i < ntraj; ++i)
	{
		trajState = nsim->getTrajectoryState(i);
		switch(trajState)
		{
			case NeuroSim::TS_SILENT:
			{
				R = 1.0; G = 0.0; B = 0.0;
				break;
			}
			case NeuroSim::TS_LOUD:
			{
				R = 1.0; G = 1.0; B = 0.0;
				break;
			}
			case NeuroSim::TS_KREGION:
			{
				R = 0.0; G = 1.0; B = 0.0;
				break;
			}
			case NeuroSim::TS_TREGION:
			{
				R = 0.0; G = 0.0; B = 1.0;
				break;
			}
			case NeuroSim::TS_DEFAULT:
			{
				R = 0.8; G = 0.8; B = 0.8;
				break;
			}
		}
		glColor3f(R,G,B);
		glVertex3f(nsim->getActivityExc(i,0),nsim->getActivityExc(i,1),nsim->getActivityExc(i,2));
	}
	glEnd();
	glPointSize(1.0);
}

void drawImage16x16(NeuroSim* nsim)
{
    int neuronIdx;
    float activity;
    float R,G,B;
    float width = 1.0/16.0;
    float spacing = 0.01;
    glBegin(GL_QUADS);
    for(int x = 0; x < 16; x++)
    {
        for(int y = 0; y < 16; ++y)
        {
            neuronIdx = x + 16*y;
            activity = nsim->getActivityExc(0,neuronIdx);
            hueColor(R,G,B,activity,0.5);
            glColor3f(R,G,B);
			glColor3f(activity,activity,activity);
            glVertex3f((x+spacing)*width,(y+spacing)*width,0.0);
            glVertex3f((x+1-spacing)*width,(y+spacing)*width,0.0);
            glVertex3f((x+1-spacing)*width,(y+1-spacing)*width,0.0);
            glVertex3f((x+spacing)*width,(y+1-spacing)*width,0.0);
        }
    }
    glEnd();
}

void drawFixedPointSpace(NeuroSim* nsim)
{
    FixedPointAnalyzer* fpAnalyzer = nsim->getFixedPointAnalyzer();
    int ntraj = nsim->getNumberOfTrajectories();
	int nneur = nsim->getNumberOfNeurons();
    int nFP = fpAnalyzer->getNumberOfFixedPoints();
    
	float R,G,B;

	drawLineAxes();

	glPointSize(50.0);
	glBegin(GL_POINTS);
	for(int i = 0; i < nFP; ++i)
	{
        IndexColord(R,G,B,i,1.0);
		glColor3f(R,G,B);
        glVertex3f((fpAnalyzer->getFixedPoint(i))[0],(fpAnalyzer->getFixedPoint(i))[1],(fpAnalyzer->getFixedPoint(i))[2]);
	}
	glEnd();
	
    glPointSize(4.0);
    glBegin(GL_POINTS);
	for(int i = 0; i < ntraj; ++i)
	{
        IndexColord(R,G,B,fpAnalyzer->getSampleToFixedPoint(i),0.4);
		glColor3f(R,G,B);
        glVertex3f(nsim->getInitialConditionsExc(i,0),nsim->getInitialConditionsExc(i,1),nsim->getInitialConditionsExc(i,2));
	}
	glEnd();
}

void drawNonLinearity(NonLinearFilter* GI, NonLinearFilter* GE)
{
	int numPoints = 1000;

	glColor3f(0.0,0.0,0.0);

	glLineWidth(3.0);
	glBegin(GL_LINES);
	for(int i = 0; i < numPoints; ++i)
	{
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(1.0,0.0,0.0);
	}
	glEnd();
    
    glLineWidth(6.0);
    
    glBegin(GL_LINE_STRIP);
	for(int i = 0; i < numPoints; ++i)
	{
        glColor3f(100.0/255.0,100.0/255.0,255.0/255.0);
		glVertex3f(1.0*i/numPoints, GI->evaluate(1.0*i/numPoints),0.0);
	}
	glEnd();

	
	glBegin(GL_LINE_STRIP);
	for(int i = 0; i < numPoints; ++i)
	{
        glColor3f(0.0,190.0/255.0,0.0);
		glVertex3f(1.0*i/numPoints, GE->evaluate(1.0*i/numPoints),0.0);
	}
	glEnd();

    glBegin(GL_LINE_STRIP);
	for(int i = 0; i < numPoints; ++i)
	{
        glColor3f(0.7,0.7,0.7);
		//glVertex3f(1.0*i/numPoints,-1.0*i/numPoints + (GE->evaluate(1.0*i/numPoints)) - (GI->evaluate(1.0*i/numPoints)),0.0);
	}
	glEnd();
}

void drawJacobianPerp(NonLinearFilter* GI, NonLinearFilter* GE,NeuroSim* nsim)
{
	int numPoints = 1000;
    int N = nsim->getNumberOfNeurons();

	glColor3f(0.0,0.0,0.0);

	glLineWidth(3.0);
	glBegin(GL_LINES);
	for(int i = 0; i < numPoints; ++i)
	{
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(1.0,0.0,0.0);
	}
	glEnd();

	glLineWidth(6.0);
	glBegin(GL_LINE_STRIP);
	for(int i = 0; i < numPoints; ++i)
	{
        if((-1.0 - (GE->evaluateDerivative(1.0*i/numPoints))/(N-1) + (GI->evaluateDerivative(1.0*i/numPoints))/(N-1)) > 0.0){glColor3f(1.0,0.0,0.0);}
        else{glColor3f(0.0,0.0,0.0);}
        glVertex3f(1.0*i/numPoints,0.1*(-1.0 - (GE->evaluateDerivative(1.0*i/numPoints))/(N-1) + (GI->evaluateDerivative(1.0*i/numPoints))/(N-1)),0.0);
	}
	glEnd();
}

void drawXofR(NonLinearFilter* GI, NonLinearFilter* GE,NeuroSim* nsim)
{
	int numPoints = 1000;
    int N = nsim->getNumberOfNeurons();
	float* XofR;
	float max,min;
	float r;

	XofR = new float[numPoints];

	for(int i = 0; i < numPoints; ++i)
	{
		r = 1.0*i/numPoints;
		max = 0.0;
		max = 2.0;
		XofR[i] = 0.5;
		for(int n = 0; n < 200; ++n)
		{
			if(-1.0*XofR[i] + 0.0*(GE->evaluate(XofR[i])) - (GI->evaluate(XofR[i])) + r > 0.0)
			{
				min = XofR[i];
			}
			else
			{
				max = XofR[i];
			}
			XofR[i] = (max+min)/2.0;
		}
	}

	glColor3f(0.0,0.0,0.0);

	glLineWidth(3.0);
	glBegin(GL_LINES);
	for(int i = 0; i < numPoints; ++i)
	{
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(1.0,0.0,0.0);
	}
	glEnd();

	glLineWidth(5.0);
	glBegin(GL_LINE_STRIP);
	for(int i = 0; i < numPoints; ++i)
	{
        if((-1.0 - 0.0*(GE->evaluateDerivative(XofR[i]))/(N-1) + (GI->evaluateDerivative(XofR[i]))/(N-1)) > 0.0){glColor3f(1.0,0.0,0.0);}
        else{glColor3f(0.0,0.0,0.0);}
		glVertex3f(1.0*i/numPoints,XofR[i],0.0);
	}
	glEnd();

	delete [] XofR;
}

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////

void PicoRenderer::initialize(HWND hwndin,int widthin,int heightin)
{
	int pixelFormat;
	
	_width = widthin;
	_height = heightin;
	_aspect = float(_width)/_height;
	_hDC = GetDC(hwndin);

	PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),  /* size */
        1,                              /* version */
        PFD_SUPPORT_OPENGL |
        PFD_DRAW_TO_WINDOW |
        PFD_DOUBLEBUFFER,               /* support double-buffering */
        PFD_TYPE_RGBA,                  /* color type */
        16,                             /* prefered color depth */
        0, 0, 0, 0, 0, 0,               /* color bits (ignored) */
        0,                              /* no alpha buffer */
        0,                              /* alpha bits (ignored) */
        0,                              /* no accumulation buffer */
        0, 0, 0, 0,                     /* accum bits (ignored) */
        16,                             /* depth buffer */
        0,                              /* no stencil buffer */
        0,                              /* no auxiliary buffers */
        PFD_MAIN_PLANE,                 /* main layer */
        0,                              /* reserved */
        0, 0, 0,                        /* no layer, visible, damage masks */
    };
    pixelFormat = ChoosePixelFormat(_hDC, &pfd);
	SetPixelFormat(_hDC, pixelFormat, &pfd);

	_hGLRC = wglCreateContext(_hDC);
	wglMakeCurrent(_hDC, _hGLRC);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glViewport(0, 0, _width, _height);

    AVIx = _width;
    AVIy = _height;
    TCHAR nameavi[] = TEXT("movie.avi");
    AviGen.SetRate(60); //fps
	AviGen.SetFileName(nameavi);
    memset(&bih,NULL,sizeof(tagBITMAPINFOHEADER));
	bih.biBitCount = 24;
	bih.biHeight = AVIy;
	bih.biWidth = AVIx;
	bih.biPlanes = 1;
	bih.biCompression = BI_RGB;
	bih.biSize = sizeof(tagBITMAPINFOHEADER);
	bih.biSizeImage = AVIx*AVIy*3;
	bih.biXPelsPerMeter = 2835;
	bih.biYPelsPerMeter = 2835;
	AviGen.SetBitmapHeader(&bih);// get bitmap info out of the view
	//AviGen.InitEngine();	// start engine
    bmBits = new BYTE[AVIx*AVIy*3];
    memset(bmBits,0,AVIx*AVIy*3);
    movieMake = 0;
}

void PicoRenderer::update(PicoSimulation* xsimulation)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
		
	glPushMatrix();
	cameraView(xsimulation->getxCamera());	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_LIGHTING);

	switch(xsimulation->getRenderState())
	{
		case GameState::RS_COLOR:
		{
            //glMatrixMode(GL_PROJECTION);
	        //glLoadIdentity();
            //glOrtho(0.0,1.0,0.0,1.0,0.1,100.0);

            glMatrixMode(GL_MODELVIEW);
	        glLoadIdentity();

			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
			//glTranslatef(0.0,0.0,-1.0);
			glScalef(10.0,10.0,10.0);
			glTranslatef(-0.5,-0.5,-0.5);
			drawActivitySpacePump(xsimulation->getNeuroSim());
            //drawImage16x16(xsimulation->getNeuroSim());
			glPopMatrix();
			break;
		}
		case GameState::RS_REGIONS:
		{
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
			glTranslatef(-5.0,-5.0,-5.0);
			glScalef(10.0,10.0,10.0);
			drawActivitySpaceRegion(xsimulation->getNeuroSim());
			glPopMatrix();
			break;
		}
		case GameState::RS_FIXEDPOINTS:
		{
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
			glTranslatef(-5.0,-5.0,-5.0);
			glScalef(10.0,10.0,10.0);
            drawFixedPointSpace(xsimulation->getNeuroSim());
			glPopMatrix();
			break;
		}
		case GameState::RS_NONLINEARCURVES:
		{
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluOrtho2D(0.0,1.0,0.0,1.0);

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			glPushMatrix();
			glTranslatef(0.0,0.5,0.0);
			glScalef(1.0,0.5,1.0);

            glPushMatrix();
            glTranslatef(0.0,0.0,0.0);
            glScalef(1.0,0.5,1.0);
			//drawNonLinearity(xsimulation->getNeuroSim()->getNonLinearResponseInh(),xsimulation->getNeuroSim()->getNonLinearResponseExc());
			//drawJacobianPerp(xsimulation->getNeuroSim()->getNonLinearResponseInh(),xsimulation->getNeuroSim()->getNonLinearResponseExc(),xsimulation->getNeuroSim());
			drawXofR(xsimulation->getNeuroSim()->getNonLinearResponseInh(),xsimulation->getNeuroSim()->getNonLinearResponseExc(),xsimulation->getNeuroSim());
            glPopMatrix();
            
            glPopMatrix();
		}

	}

	glEnable(GL_LIGHTING);
	glPopMatrix();
	glFlush();
	SwapBuffers(_hDC);
    
    if(movieMake)
    {
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
	    glReadPixels(0,0,AVIx,AVIy,GL_BGR_EXT,GL_UNSIGNED_BYTE,bmBits);
        AviGen.AddFrame(bmBits);

        if(xsimulation->getUpdateCounter() > 1800)
        {
            movieMake = 0;
            AviGen.ReleaseEngine();
        }
    }
}

void PicoRenderer::cameraView(PicoCamera* xcam)
{
	float angle;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(xcam->_FoV, _aspect, 0.1, 1000.0);

	glTranslatef(0.0,0.0,-xcam->_distance);
	glRotatef(xcam->_elevation,1.0,0.0,0.0);
	glRotatef(-xcam->_azimuth,0.0,1.0,0.0);
	glTranslatef(-xcam->_position.x,-xcam->_position.y,-xcam->_position.z);
}

void PicoRenderer::release()
{
}

