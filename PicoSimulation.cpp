#include "PicoSimulation.h"

void GreyBitmapLoader::contrastNormalize()
{
    float Pmax,Pmin;
    float A,B;

    Pmax = 0;
    Pmin = 1.0;
    for(int p = 0; p < width*height; ++p)
    {
        if(pixels[p] > Pmax){Pmax = pixels[p];}
        if(pixels[p] < Pmin){Pmin = pixels[p];}
    }

    if(Pmax - contrast > 0.5)
    {A = 0.5/(Pmax-contrast);}
    else if(contrast - Pmin > 0.5)
    {A = 0.5/(contrast - Pmin);}
    else
    {A = 1.0;}

    B = 0.5 - contrast;

    for(int p = 0; p < width*height; ++p)
    {
        pixels[p] = A*pixels[p] + B;
    }
    contrast = 0.5;
}

void GreyBitmapLoader::initialize(const char *fname)
{
	FILE *fin;
	unsigned char buffer[256];
	fin = fopen(fname,"rb");
	
	fread(buffer,1,10,fin);
	fread(buffer,4,1,fin);
	fread(buffer,4,1,fin);
	fread(buffer,4,2,fin);

	width = int(buffer[0]);
	height = int(buffer[4]);

	fread(buffer,2,2,fin);

	

	pixels = new float[width*height];

	fseek(fin,54,SEEK_SET);
    contrast = 0;
	for(int p = 0; p < width*height; ++p)
	{
		fread(buffer,3,1,fin);
		pixels[p] = buffer[0]/255.0;
        contrast += pixels[p];
	}
    contrast /= (width*height);
    
    printf("%d - %d, contrast = %f\n",width,height,contrast);

	fclose(fin);

    contrastNormalize();
}

void OutputLoggerFP::initialize(const char* fpstatname, const char* fpparamname, NeuroSim *ns)
{
    sprintf(fpStatFileName,"%s",fpstatname);
    sprintf(fpParamFileName,"%s",fpparamname);

    numberOfSimsLogged = 0;
    ns->setupNewSimulationRandom();
}

void OutputLoggerFP::pushToFiles(NeuroSim* ns)
{
    fpStat = fopen(fpStatFileName,"a");
    fpParam = fopen(fpParamFileName,"a");

    for(int n = 0; n < maxNumberOfSims; ++n)
    {
        fprintf(fpStat,"%d",numberOfFP[n]);
        if(n == maxNumberOfSims-1)
        {fprintf(fpStat,"\n");}
        else
        {fprintf(fpStat,",");}
    }

    /*fprintf(fpParam,"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                    ns->getNonLinearResponseExc()->getThreshold(),
                    ns->getNonLinearResponseExc()->getSaturation(),
                    ns->getNonLinearResponseExc()->getSteepness(),
                    ns->getNonLinearResponseInh()->getThreshold(),
                    ns->getNonLinearResponseInh()->getSaturation(),
                    ns->getNonLinearResponseInh()->getSteepness(),
                    ns->getWeightExc(0,1),
                    ns->getWeightExc(0,2),
                    ns->getWeightExc(1,0),
                    ns->getWeightExc(1,2),
                    ns->getWeightExc(2,0),
                    ns->getWeightExc(2,1),
                    ns->getWeightInh(0,1),
                    ns->getWeightInh(0,2),
                    ns->getWeightInh(1,0),
                    ns->getWeightInh(1,2),
                    ns->getWeightInh(2,0),
                    ns->getWeightInh(2,1));*/
    fclose(fpStat);
    fclose(fpParam);
}

void OutputLoggerFP::logSimulation(NeuroSim* ns)
{
    ns->findFixedPoints();
    numberOfFP[numberOfSimsLogged] = ns->getFixedPointAnalyzer()->getNumberOfFixedPoints();
    printf("OutputLoggerFP :: ExternalDrive = %f, Number of FP = %d\n",ns->getExternalDrive(0,0),numberOfFP[numberOfSimsLogged]);
    numberOfSimsLogged++;

    if(numberOfSimsLogged == maxNumberOfSims)
    {
        pushToFiles(ns);
        numberOfSimsLogged = 0;
        ns->setupNewSimulationRandom();
        
    }
    else{}
    
    for(int t = 0; t < ns->getNumberOfTrajectories(); ++t)
    {
        for(int n = 0; n < ns->getNumberOfNeurons(); ++n)
        {
            ns->setExternalDrive(t,n,(1.0*numberOfSimsLogged)/maxNumberOfSims);
        }
    }
    ns->resetTrajectories();
}

void OutputLoggerNoisySynapse::initialize(const char* fpnoisename, NeuroSim* ns)
{
    sprintf(fpNoiseFileName,"%s",fpnoisename);

    numberOfSimsLogged = 0;
    currentNoiseLevel = ns->setupNewSimulationNoisySynapses();
}

void OutputLoggerNoisySynapse::pushToFiles(NeuroSim* ns)
{
    fpNoise = fopen(fpNoiseFileName,"a");

    fprintf(fpNoise,"%f,",currentNoiseLevel);
    for(int n = 0; n < maxNumberOfSims; ++n)
    {
        fprintf(fpNoise,"%d",numberOfFP[n]);
        if(n == maxNumberOfSims-1)
        {fprintf(fpNoise,"\n");}
        else
        {fprintf(fpNoise,",");}
    }

    fclose(fpNoise);
}

void OutputLoggerNoisySynapse::logSimulation(NeuroSim* ns)
{
    ns->findFixedPoints();
    numberOfFP[numberOfSimsLogged] = ns->getFixedPointAnalyzer()->getNumberOfFixedPoints();
    numberOfSimsLogged++;

    if(numberOfSimsLogged == maxNumberOfSims)
    {
        pushToFiles(ns);
        numberOfSimsLogged = 0;
        currentNoiseLevel = ns->setupNewSimulationNoisySynapses();   
    }
    else{}
    
    for(int t = 0; t < ns->getNumberOfTrajectories(); ++t)
    {
        for(int n = 0; n < ns->getNumberOfNeurons(); ++n)
        {
            ns->setExternalDrive(t,n,(1.0*numberOfSimsLogged)/maxNumberOfSims);
        }
    }
    ns->resetTrajectories();
}

void OutputLoggerPerturb::initialize(const char* fpnoisename, const char* dirfilename, NeuroSim* ns)
{
    int line = 0;

    sprintf(fpPerturbFileName,"%s",fpnoisename);
    sprintf(directionFileName,"%s",dirfilename);

    printf("Enter BlockNumber:");
    blockNumber = getchar()-48;
    numberOfDirectionsTried = 0;
    magnitude = 0.17; //10% of the total magnitude of the matrix vector

    dirFile = fopen(directionFileName,"r");
    while(line <= 1000*blockNumber+numberOfDirectionsTried)
    {
        fgets(buffer,256,dirFile);
        line++;
    }
    fclose(dirFile);

    sscanf(buffer,"%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                   &line,&direction[0],&direction[1],&direction[2],
                         &direction[3],&direction[4],&direction[5],
                         &direction[6],&direction[7],&direction[8],
                         &direction[9],&direction[10],&direction[11]);

    printf("line is %d\n",line);

    numberOfSimsLogged = 0;
    ns->setupNewSimulationPerturb(direction,magnitude);
    for(int t = 0; t < ns->getNumberOfTrajectories(); ++t)
    {
        for(int n = 0; n < ns->getNumberOfNeurons(); ++n)
        {
            ns->setExternalDrive(t,n,(1.0*numberOfSimsLogged)/maxNumberOfSims);
        }
    }
    ns->resetTrajectories();
}

void OutputLoggerPerturb::logSimulation(NeuroSim* ns)
{
    int line = 0;

    ns->findFixedPoints();
    numberOfFP[numberOfSimsLogged] = ns->getFixedPointAnalyzer()->getNumberOfFixedPoints();
    numberOfSimsLogged++;

    if(numberOfSimsLogged == maxNumberOfSims)
    {
        pushToFiles(ns);

        numberOfDirectionsTried++;
        if(numberOfDirectionsTried == 1000)
        {system("PAUSE");}
        else
        {
            dirFile = fopen(directionFileName,"r");
            while(line <= 1000*blockNumber+numberOfDirectionsTried)
            {
               fgets(buffer,256,dirFile);
               line++;
            }
            fclose(dirFile);
        }

        sscanf(buffer,"%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                   &line,&direction[0],&direction[1],&direction[2],
                         &direction[3],&direction[4],&direction[5],
                         &direction[6],&direction[7],&direction[8],
                         &direction[9],&direction[10],&direction[11]);

        printf("line is %d\n",line);

        numberOfSimsLogged = 0;
        ns->setupNewSimulationPerturb(direction,magnitude);
    }
    else{}
    
    for(int t = 0; t < ns->getNumberOfTrajectories(); ++t)
    {
        for(int n = 0; n < ns->getNumberOfNeurons(); ++n)
        {
            ns->setExternalDrive(t,n,(1.0*numberOfSimsLogged)/maxNumberOfSims);
        }
    }
    ns->resetTrajectories();
}

void OutputLoggerPerturb::pushToFiles(NeuroSim* ns)
{
    fpNoise = fopen(fpPerturbFileName,"a");

    fprintf(fpNoise,"%d,",1000*blockNumber+numberOfDirectionsTried);
    for(int n = 0; n < maxNumberOfSims; ++n)
    {
        fprintf(fpNoise,"%d",numberOfFP[n]);
        if(n == maxNumberOfSims-1)
        {fprintf(fpNoise,"\n");}
        else
        {fprintf(fpNoise,",");}
    }

    fclose(fpNoise);
}

void OutputLoggerNofR::initialize(const char* fpNofRname, NeuroSim* ns)
{
    sprintf(fpNofRFileName,"%s",fpNofRname);

    numberOfSimsLogged = 0;
    for(int t = 0; t < ns->getNumberOfTrajectories(); ++t)
    {
        for(int n = 0; n < ns->getNumberOfNeurons(); ++n)
        {
            ns->setExternalDrive(t,n,(1.0*numberOfSimsLogged)/maxNumberOfSims);
        }
    }
}

void OutputLoggerNofR::logSimulation(NeuroSim* ns)
{
    ns->findFixedPoints();
    numberOfFP[numberOfSimsLogged] = ns->getFixedPointAnalyzer()->getNumberOfFixedPoints();
    numberOfSimsLogged++;

    if(numberOfSimsLogged == maxNumberOfSims)
    {
        pushToFiles(ns);
        system("PAUSE");
    }
    else{}
    
    for(int t = 0; t < ns->getNumberOfTrajectories(); ++t)
    {
        for(int n = 0; n < ns->getNumberOfNeurons(); ++n)
        {
            ns->setExternalDrive(t,n,(1.0*numberOfSimsLogged)/maxNumberOfSims);
        }
    }
    ns->resetTrajectories();
}

void OutputLoggerNofR::pushToFiles(NeuroSim* ns)
{
    fpNofR = fopen(fpNofRFileName,"a");

    for(int n = 0; n < maxNumberOfSims; ++n)
    {
        fprintf(fpNofR,"%d",numberOfFP[n]);
        if(n == maxNumberOfSims-1)
        {fprintf(fpNofR,"\n");}
        else
        {fprintf(fpNofR,",");}
    }

    fclose(fpNofR);
}

void PicoSimulation::setNeuroSimToImage(GreyBitmapLoader *gbit, NeuroSim* nsim)
{
	int neurons = nsim->getNumberOfNeurons();
	float* pix = gbit->getPixels();

	for(int N = 0; N < neurons; ++N)
	{
		nsim->setActivityExc(0,N,pix[N]);
	}
}

void PicoSimulation::initialize()
{
    char bufferA[256];
    char bufferB[256];

	_timeAccum = 0.0;
	_mouseSensitivity = 0.5;

	simPause = 1;
	gameState.currentState = GameState::GS_SIMULATIONVIEW;
	gameState.currentRenderState = GameState::RS_COLOR;

	neuroSim.initialize("Simulation.cfg");

	gBitmapLoader.initialize("smallSky.bmp");
	//setNeuroSimToImage(&gBitmapLoader, &neuroSim);
    gBitmapLoader2.initialize("smallEye.bmp");

    //sprintf(bufferA,"fpStats%d.csv",neuroSim.getSeed());
    //sprintf(bufferB,"fpParam%d.csv",neuroSim.getSeed());

    //sprintf(bufferA,"fpNoise%d.csv",neuroSim.getSeed());

    //sprintf(bufferA,"fpPerturb.csv");
    //sprintf(bufferB,"VecDataCartesian.csv");

    sprintf(bufferA,"fpNofR.csv");

   //outputLogger.initialize(bufferA,&neuroSim);

	//TEMPORARY FOR TESTING
	_cam._position.setToZero();

	_cam._distance = 15.0;
	_cam._azimuth = 0.0;
	_cam._elevation = 25.0;

	_cam._FoV = 75.0;
	/////////////////////

    TCHAR nameavi[] = TEXT("movie.avi");

    AviGen.SetRate(30);	// set 30fps
	AviGen.SetFileName(nameavi);

    memset(&bih,NULL,sizeof(tagBITMAPINFOHEADER));
	bih.biBitCount = 24;
	bih.biHeight = 1024;
	bih.biWidth = 1024;
	bih.biPlanes = 1;
	bih.biCompression = BI_RGB;
	bih.biSize = sizeof(tagBITMAPINFOHEADER);
	bih.biSizeImage = 1024*1024;
	bih.biXPelsPerMeter = 2835;
	bih.biYPelsPerMeter = 2835;

	AviGen.SetBitmapHeader(&bih);// get bitmap info out of the view
	AviGen.InitEngine();	// start engine

    bmBits = new GLuint[1024*1024*3];
	memset(bmBits,0,1024*1024*3);

    updateCounter = 0;
    frameNumber = 0;
}

void PicoSimulation::update(PicoInput* xinput,float dtin)
{
	_timeAccum += dtin;
	if(_timeAccum > 0.5){_timeAccum = 0.5;}

	switch(gameState.currentState)
	{
		case GameState::GS_SIMULATIONVIEW:
		{
			if(!simPause)
			{
                neuroSim.update(10);
                if(neuroSim.getSimulationTime() < 1000.0)
                {
                    //neuroSim.updateWeights(gBitmapLoader.getPixels());
                    //setNeuroSimToImage(&gBitmapLoader2, &neuroSim);
                    //neuroSim.updateWeights(gBitmapLoader2.getPixels());
                }
                //else{printf("doneTraining\n");}
            }
			
			if(xinput->getKeyWasPressed('R'))
			{neuroSim.resetTrajectories();}

			if(xinput->getKeyWasPressed('P'))
			{simPause = !simPause;}

            if(xinput->getKeyWasPressed(VK_UP))
            {neuroSim.setExternalDriveAll(neuroSim.getExternalDrive(0,0)+1.0);}
            if(xinput->getKeyWasPressed(VK_DOWN))
			{neuroSim.setExternalDriveAll(neuroSim.getExternalDrive(0,0)-1.0);}

			if(xinput->getKeyWasPressed('T'))
			{
				if(gameState.currentRenderState == GameState::RS_COLOR)
				{gameState.currentRenderState = GameState::RS_REGIONS;}
				else if(gameState.currentRenderState == GameState::RS_REGIONS)
				{gameState.currentRenderState = GameState::RS_NONLINEARCURVES;}
				else if(gameState.currentRenderState = GameState::RS_NONLINEARCURVES)
				{gameState.currentRenderState = GameState::RS_COLOR;}
			}

			if(xinput->getKeyWasPressed('G'))
			{
                simPause = 1;
				gameState.currentState = GameState::GS_SIMTRANSITION;
			}

			break;
		}
		case GameState::GS_SIMTRANSITION:
		{
            neuroSim.findFixedPoints();
			gameState.currentState = GameState::GS_FIXEDPOINTSVIEW;
			gameState.currentRenderState = GameState::RS_FIXEDPOINTS;
			break;
		}
		case GameState::GS_FIXEDPOINTSVIEW:
		{
			if(xinput->getKeyWasPressed('G'))
			{
				gameState.currentState = GameState::GS_SIMULATIONVIEW;
				gameState.currentRenderState = GameState::RS_COLOR;
                simPause = 0;
			}
			break;
		}
	}
    if(xinput->getKeyState(VK_RBUTTON))
    {
	    _cam._azimuth += _mouseSensitivity*(-xinput->getDeltaMposX());
	    _cam._elevation += _mouseSensitivity*(-xinput->getDeltaMposY());
	    _cam._distance += -xinput->getDeltaMwheel();
    }

    if((neuroSim.getSimulationTime() > 5000.0)&&(frameNumber < 1000))
    {
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
	    glReadPixels(0,0,1024,1024,GL_BGR_EXT,GL_UNSIGNED_BYTE,bmBits);
        AviGen.AddFrame((BYTE*)bmBits);

        neuroSim.resetTrajectories();
        neuroSim.resetExternalDriveRandom(frameNumber*0.001,frameNumber*0.001+0.05);

        //outputLogger.logSimulation(&neuroSim);
        printf("frame %d logged\n",frameNumber);
        frameNumber++;
    }
    if(frameNumber >= 1000)
    {
        printf("Movie Complete\n");
    }

   // _cam._azimuth -= 0.2;

    updateCounter++;
}

void PicoSimulation::release()
{
    AviGen.ReleaseEngine();
	neuroSim.release();
}