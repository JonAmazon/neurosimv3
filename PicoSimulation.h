#ifndef _H_PICOSIMULATION
#define _H_PICOSIMULATION

#include "PicoGlobal.h"
#include "PicoInput.h"
#include "PicoVec.h"
#include "NeuroSim.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include "AVIGenerator.h"

struct PicoCamera
{
	float _FoV,_distance;
	float _azimuth,_elevation;
	PicoVec4f _position;
};

struct GameState
{
	enum GameStateEnum 
	{
		GS_SIMULATIONVIEW,
		GS_SIMTRANSITION,
		GS_FIXEDPOINTSVIEW,
	};

	enum RenderStateEnum
	{
		RS_COLOR,
		RS_REGIONS,
		RS_FIXEDPOINTS,
		RS_NONLINEARCURVES
	};

	GameStateEnum currentState;
	RenderStateEnum currentRenderState;
};

class GreyBitmapLoader
{
private:
	int width,height;
    float contrast;
	float *pixels;

    void contrastNormalize();
public:
	void initialize(const char *fname);
    float getContrast(){return contrast;}
	float* getPixels(){return &pixels[0];}
};

class OutputLoggerFP
{
private:
    char fpStatFileName[256];
    char fpParamFileName[256];
    int numberOfSimsLogged;
    static const int maxNumberOfSims = 100;
    int numberOfFP[maxNumberOfSims];
    FILE *fpStat,*fpParam;

    void pushToFiles(NeuroSim* ns);
public:
    void initialize(const char* fpstatname, const char* fpparamname, NeuroSim *ns);
    void logSimulation(NeuroSim* ns);
};

class OutputLoggerNoisySynapse
{
private:
    char fpNoiseFileName[256];
    int numberOfSimsLogged;
    static const int maxNumberOfSims = 100;
    int numberOfFP[maxNumberOfSims];
    float currentNoiseLevel;
    FILE *fpNoise;

    void pushToFiles(NeuroSim* ns);
public:
    void initialize(const char* fpnoisename, NeuroSim* ns);
    void logSimulation(NeuroSim* ns);
};

class OutputLoggerPerturb
{
private:
    char fpPerturbFileName[256];
    char directionFileName[256];
    char buffer[256];
    int numberOfSimsLogged;
    static const int maxNumberOfSims = 100;
    int numberOfFP[maxNumberOfSims];

    int blockNumber;
    int numberOfDirectionsTried;
    float direction[12];
    float magnitude;

    FILE *dirFile;
    FILE *fpNoise;

    void pushToFiles(NeuroSim* ns);
public:
    void initialize(const char* fpnoisename, const char* dirfilename, NeuroSim* ns);
    void logSimulation(NeuroSim* ns);
};

class OutputLoggerNofR
{
private:
    char fpNofRFileName[256];
    char buffer[256];
    int numberOfSimsLogged;
    static const int maxNumberOfSims = 100;
    int numberOfFP[maxNumberOfSims];

    FILE *fpNofR;

    void pushToFiles(NeuroSim* ns);
public:
    void initialize(const char* fpNofRname, NeuroSim* ns);
    void logSimulation(NeuroSim* ns);
};

class PicoSimulation
{
public:
	void initialize();
	void update(PicoInput* xinput,float dt);
	void release();

    int getUpdateCounter(){return updateCounter;}
	PicoCamera* getxCamera(){return &_cam;}
	NeuroSim* getNeuroSim(){return &neuroSim;}
	GameState::RenderStateEnum getRenderState(){return gameState.currentRenderState;}
private:
	int simPause;
    int updateCounter,frameNumber;
	float _timeAccum;
	float _mouseSensitivity;

	GameState gameState;
	PicoCamera _cam;
	NeuroSim neuroSim;
	GreyBitmapLoader gBitmapLoader;
    GreyBitmapLoader gBitmapLoader2;
    OutputLoggerNofR outputLogger;
    
    CAVIGenerator AviGen;
	tagBITMAPINFOHEADER bih;
    GLuint* bmBits;

	void setNeuroSimToImage(GreyBitmapLoader *gbit, NeuroSim* nsim);
};

#endif