#include "NeuroSim.h"

float NormalDeviate(float mean, float stdDev, stdRan* random)
{
    int foundValue = 0;
    float tryX,tryY;

    while(!foundValue)
    {
        tryX = 200.0*(random->doub())-100.0;
        tryY = 1.0*(random->doub());
        if(tryY < expf(-tryX*tryX/2.0))
        {
            foundValue = 1;
        }
    }

    return tryX*stdDev+mean;
}

void FixedPointAnalyzer::initialize(int numberOfSamplesIn,int dimIn,float toleranceIn)
{
    numberOfFixedPoints = 0;
    numberOfSamples = numberOfSamplesIn;
    dim = dimIn;
    tolerance = toleranceIn;

    sampleToFixedPoint = new int[numberOfSamples];
    fixedPoints = new float*[numberOfSamples];
    for(int s = 0; s < numberOfSamples; ++s)
    {
        fixedPoints[s] = new float[dim];
    }
}

void FixedPointAnalyzer::findFixedPoints(float **samples)
{
    int fixedPointAlreadyFound;
    
    numberOfFixedPoints = 0;
    for(int s = 0; s < numberOfSamples; ++s)
    {
        fixedPointAlreadyFound = 0;
        for(int f = 0; f < numberOfFixedPoints; ++f)
        {
            if(distanceL2(samples[s],fixedPoints[f]) < tolerance)
            {
                fixedPointAlreadyFound = 1;
                sampleToFixedPoint[s] = f;
            }
        }

        if(!fixedPointAlreadyFound)
        {
            for(int i = 0; i < dim; ++i)
            {
                fixedPoints[numberOfFixedPoints][i] = samples[s][i];
            }
            sampleToFixedPoint[s] = numberOfFixedPoints;
            numberOfFixedPoints++;
        }
    }
}

void FixedPointAnalyzer::release()
{
    delete [] sampleToFixedPoint;
    for(int s = 0; s < numberOfSamples; ++s)
    {
        delete [] fixedPoints[s];
    }
    delete [] fixedPoints;
}

void NeuroSim::loadMatrixFromFile(float **M, float mult, const char *file)
{

    FILE *fin = fopen(file,"r");

    for(int r = 0; r < numberOfNeurons; ++r)
    {
        for(int c = 0; c < numberOfNeurons; ++c)
        {
            fscanf(fin,"%f,",&M[r][c]);
            M[r][c] *= mult;
        }
    }
}

void NeuroSim::initialize(const char *paramf)
{
    char weightEEfile[256];
    char weightEIfile[256];
    char weightIEfile[256];
    char weightIIfile[256];
    NonLinearFilter::FilterType filterType;
    float A,B,C,D;
    float multEE,multEI,multIE,multII;
    FILE *paramin = fopen(paramf,"r");

    fscanf(paramin,"numberOfNeurons = %d\n",&numberOfNeurons);
    printf("NeuroSim::initialize - Number Of Neurons = %d\n",numberOfNeurons);
    fscanf(paramin,"numberOfTrajectories = %d\n",&numberOfTrajectories);
    printf("NeuroSim::initialize - Number Of Trajectories = %d\n",numberOfTrajectories);

    initialConditionsExc = new float*[numberOfTrajectories];
	activityExc = new float*[numberOfTrajectories];
	activityInh = new float*[numberOfTrajectories];
	inputExc = new float*[numberOfTrajectories];
	inputInh = new float*[numberOfTrajectories];
	externalDrive = new float*[numberOfTrajectories];
	weightEE = new float*[numberOfNeurons];
	weightEI = new float*[numberOfNeurons];
    weightEI = new float*[numberOfNeurons];
    weightIE = new float*[numberOfNeurons];
    weightII = new float*[numberOfNeurons];
	trajectoryStates =  new TrajectoryStateEnum[numberOfTrajectories];

	for(int i = 0; i < numberOfTrajectories; ++i)
	{
        initialConditionsExc[i] = new float[numberOfNeurons];
		activityExc[i] = new float[numberOfNeurons];
		activityInh[i] = new float[numberOfNeurons];
		inputExc[i] = new float[numberOfNeurons];
		inputInh[i] = new float[numberOfNeurons];
        externalDrive[i] = new float[numberOfNeurons];
		trajectoryStates[i] = TS_DEFAULT;
	}

	for(int i = 0; i < numberOfNeurons; ++i)
	{
		weightEE[i] = new float[numberOfNeurons];
		weightEI[i] = new float[numberOfNeurons];
        weightIE[i] = new float[numberOfNeurons];
        weightII[i] = new float[numberOfNeurons];
	}

	k1Exc = new float[numberOfNeurons];
	k2Exc = new float[numberOfNeurons];
	k3Exc = new float[numberOfNeurons];
	k4Exc = new float[numberOfNeurons];

	k1Inh = new float[numberOfNeurons];
	k2Inh = new float[numberOfNeurons];
	k3Inh = new float[numberOfNeurons];
	k4Inh = new float[numberOfNeurons];

    fscanf(paramin,"timeStep = %f\n",&timeStep);
    printf("NeuroSim::initialize - Time Step = %f ms\n",timeStep);

    fscanf(paramin,"\n:: EXCITATORY PARAMETERS ::\n");
    printf("\n:: EXCITATORY PARAMETERS ::\n");

    fscanf(paramin,"tauE = %f\n",&tauExc);
    printf("NeuroSim::initialize - tauE = %f ms\n",tauExc);
    fscanf(paramin,"TypeE = %d\n",&filterType);
    printf("NeuroSim::initialize - TypeE = %d ms\n",filterType);
    fscanf(paramin,"AE = %f\n",&A);
    printf("NeuroSim::initialize - AE = %f\n",A);
    fscanf(paramin,"BE = %f\n",&B);
    printf("NeuroSim::initialize - BE = %f\n",B);
    fscanf(paramin,"CE = %f\n",&C);
    printf("NeuroSim::initialize - CE = %f\n",C);
    fscanf(paramin,"DE = %f\n",&D);
    printf("NeuroSim::initialize - DE = %f\n",D);

    switch(filterType)
    {
        case NonLinearFilter::PWL:
            {
                responseExc = new NonLinearFilterPWL;
                break;
            }
        case NonLinearFilter::EXP:
            {
                responseExc = new NonLinearFilterExp;
                break;
            }
        case NonLinearFilter::SQRT:
            {
                responseExc = new NonLinearFilterSqrt;
                break;
            }
        case NonLinearFilter::DSC:
            {
                responseExc = new NonLinearFilterDsc;
                break;
            }
    }
    responseExc->initialize(A,B,C,D);

    fscanf(paramin,"externalDriveMin = %f\n",&externalDriveMin);
    printf("NeuroSim::initialize - External Drive Min = %f\n",externalDriveMin);
    fscanf(paramin,"externalDriveMax = %f\n",&externalDriveMax);
    printf("NeuroSim::initialize - External Drive Max = %f\n",externalDriveMax);

    fscanf(paramin,"\n:: INHIBITORY PARAMETERS ::\n");
    printf("\n:: INHIBITORY PARAMETERS ::\n");

    fscanf(paramin,"tauI = %f\n",&tauInh);
    printf("NeuroSim::initialize - tauI = %f ms\n",tauInh);
    fscanf(paramin,"TypeI = %d\n",&filterType);
    printf("NeuroSim::initialize - TypeI = %d ms\n",filterType);
    fscanf(paramin,"AI = %f\n",&A);
    printf("NeuroSim::initialize - AI = %f\n",A);
    fscanf(paramin,"BI = %f\n",&B);
    printf("NeuroSim::initialize - BI = %f\n",B);
    fscanf(paramin,"CI = %f\n",&C);
    printf("NeuroSim::initialize - CI = %f\n",C);
    fscanf(paramin,"DI = %f\n",&D);
    printf("NeuroSim::initialize - DI = %f\n",D);

	switch(filterType)
    {
        case NonLinearFilter::PWL:
            {
                responseInh = new NonLinearFilterPWL;
                break;
            }
        case NonLinearFilter::EXP:
            {
                responseInh = new NonLinearFilterExp;
                break;
            }
        case NonLinearFilter::SQRT:
            {
                responseInh = new NonLinearFilterSqrt;
                break;
            }
        case NonLinearFilter::DSC:
            {
                responseInh = new NonLinearFilterDsc;
                break;
            }
    }
    responseInh->initialize(A,B,C,D);

    fscanf(paramin,"\n:: WEIGHT MATRICES ::\n");
    printf("\n:: WEIGHT MATRICES ::\n");

    fscanf(paramin,"weightEE = %s\n",weightEEfile);
    printf("NeuroSim::initialize - weightEE filename = %s\n",weightEEfile);
    fscanf(paramin,"weightEI = %s\n",weightEIfile);
    printf("NeuroSim::initialize - weightEI filename = %s\n",weightEIfile);
    fscanf(paramin,"weightIE = %s\n",weightIEfile);
    printf("NeuroSim::initialize - weightIE filename = %s\n",weightIEfile);
    fscanf(paramin,"weightII = %s\n",weightIIfile);
    printf("NeuroSim::initialize - weightII filename = %s\n",weightIIfile);

    fscanf(paramin,"multiplierEE = %f\n",&multEE);
    printf("NeuroSim::initialize - multiplierEE = %f\n",multEE);
    fscanf(paramin,"multiplierEI = %f\n",&multEI);
    printf("NeuroSim::initialize - multiplierEI = %f\n",multEI);
    fscanf(paramin,"multiplierIE = %f\n",&multIE);
    printf("NeuroSim::initialize - multiplierIE = %f\n",multIE);
    fscanf(paramin,"multiplierII = %f\n",&multII);
    printf("NeuroSim::initialize - multiplierII = %f\n",multII);

    fclose(paramin);

    loadMatrixFromFile(weightEE,multEE,weightEEfile);
    loadMatrixFromFile(weightEI,multEI,weightEIfile);
    loadMatrixFromFile(weightIE,multIE,weightIEfile);
    loadMatrixFromFile(weightII,multII,weightIIfile);

	resetWeightsStandard(1.0,1.0);

    fixedPointAnalyzer.initialize(numberOfTrajectories,numberOfNeurons,0.0001);

    seed = time(NULL);
	random = new stdRan(seed);
    resetExternalDriveRandom(externalDriveMin,externalDriveMax);
	resetActvityRandom();

    updateCounter = 0;
}

void NeuroSim::setupNewSimulationRandom()
{
    float r1,r2;
    float thresh,slope,sat;

        r1 = random->doub();
        r2 = random->doub();
        thresh = (r2>r1)?r1:r2;
        sat = (r2>r1)?r2:r1;
        slope = tanf(3.14159265359*(random->doub())/2.0);

        responseExc->initialize(thresh,sat,slope,0.0);

        r1 = random->doub();
        r2 = random->doub();
        thresh = (r2>r1)?r1:r2;
        sat = (r2>r1)?r2:r1;
        slope = tanf(3.14159265359*(random->doub())/2.0);

        responseInh->initialize(thresh,sat,slope,0.0);

    for(int i = 0; i < numberOfNeurons; ++i)
    {
        for( int j = 0; j < numberOfNeurons; ++j)
        {
            weightEE[i][j] = 0.0;
            weightEI[i][j] = 0.0;
            weightIE[i][j] = 0.0;
            weightII[i][j] = 0.0;
            if(i == j){}
            else
            {
                weightEE[i][j] = (random->doub())/(numberOfNeurons-1);
                weightII[i][j] = (random->doub())/(numberOfNeurons-1);
            }
            weightEI[i][j] = (random->doub())/(numberOfNeurons-1);
            weightIE[i][j] = (random->doub())/(numberOfNeurons-1);
        }
    }
}

float NeuroSim::setupNewSimulationNoisySynapses()
{
    float noiseLevel = random->doub();
    for(int i = 0; i < numberOfNeurons; ++i)
    {
        for( int j = 0; j < numberOfNeurons; ++j)
        {
            weightEE[i][j] = 0.0;
            weightEI[i][j] = 0.0;
            weightIE[i][j] = 0.0;
            weightII[i][j] = 0.0;
            if(i == j)
            {
                weightIE[i][j] = 1.0;
            }
            else
            {
                weightEE[i][j] = (NormalDeviate(1.0,noiseLevel,random))/(numberOfNeurons-1);
                weightEI[i][j] = (NormalDeviate(1.0,noiseLevel,random))/(numberOfNeurons-1);
            }
        }
    }

    return noiseLevel;
}

void NeuroSim::setupNewSimulationPerturb(float *dir, float mag)
{
    for(int i = 0; i < numberOfNeurons; ++i)
    {
        for( int j = 0; j < numberOfNeurons; ++j)
        {
            weightEE[i][j] = 0.0;
            weightEI[i][j] = 0.0;
            weightIE[i][j] = 0.0;
            weightII[i][j] = 0.0;
            if(i == j)
            {
                weightIE[i][j] = 1.0;
            }
            else
            {
                weightEE[i][j] = 1.0/(numberOfNeurons-1);
                weightEI[i][j] = 1.0/(numberOfNeurons-1);
            }
        }
    }

    weightEE[0][1] += dir[0]*mag;
    weightEE[0][2] += dir[1]*mag;
    weightEE[1][0] += dir[2]*mag;
    weightEE[1][2] += dir[3]*mag;
    weightEE[2][0] += dir[4]*mag;
    weightEE[2][1] += dir[5]*mag;
    weightEI[0][1] += dir[6]*mag;
    weightEI[0][2] += dir[7]*mag;
    weightEI[1][0] += dir[8]*mag;
    weightEI[1][2] += dir[9]*mag;
    weightEI[2][0] += dir[10]*mag;
    weightEI[2][1] += dir[11]*mag;
}

void NeuroSim::resetExternalDriveRandom(float a, float b)
{
    float r;

    externalDriveMin = a;
    externalDriveMax = b;
	for(int t = 0; t < numberOfTrajectories; ++t)
	{
        r = (externalDriveMax - externalDriveMin)*(random->doub());
        for(int n = 0; n < numberOfNeurons; ++n)
        {
            externalDrive[t][n] = r + externalDriveMin;
        }
	}
}

void NeuroSim::resetExternalDriveZero()
{
	for(int t = 0; t < numberOfTrajectories; ++t)
	{
        for(int n = 0; n < numberOfNeurons; ++n)
        {
		    externalDrive[t][n] = 0.0;
        }
	}
}

void NeuroSim::resetActvityRandom()
{
    updateCounter = 0;
	for(int t = 0; t < numberOfTrajectories; ++t)
	{
        trajectoryStates[t] = TS_DEFAULT;
		for(int n = 0; n < numberOfNeurons; ++n)
		{
			activityExc[t][n] = random->doub();
			activityInh[t][n] = 0.0;

			inputExc[t][n] = 0.0;
			inputInh[t][n] = 0.0;

            initialConditionsExc[t][n] = activityExc[t][n];
		}
	}
}

void NeuroSim::resetWeightsStandard(float lambdaE, float lambdaI)
{
	for(int i = 0; i < numberOfNeurons; ++i)
	{
		for(int j = 0; j < numberOfNeurons; ++j)
		{
			if(i == j){weightEE[i][j] = 0.0;}
			else{weightEE[i][j] = lambdaE/(numberOfNeurons-1);}
			if(i == j){weightEI[i][j] = 0.0;}
			else{weightEI[i][j] = lambdaI/(numberOfNeurons-1);}

            if(i == j){weightII[i][j] = 0.0;}
			else{weightII[i][j] = 0.0;}

            if(i == j){weightIE[i][j] = 1.0;}
            else{weightIE[i][j] = 0.0;}
		}
	}
}

void NeuroSim::classifyTrajectoryStates()
{
	int numberSilent,numberLoud,numberTransition;
	float perronRootInh,perronRootExc;
	float uInh,uExc;

	//GET PERRON ROOT OF WEIGHT MATRICES
	perronRootInh = 0.9;
	perronRootExc = 0.9;
	/////////////////////////////////////////////

	for(int t = 0; t < numberOfTrajectories; ++t)
	{
		numberSilent = 0;
		numberLoud = 0;
		numberTransition = 0;

		for(int n = 0; n < numberOfNeurons; ++n)
		{
			//comput inhibitory input to neuron

			if( ((-1.0 - (responseExc->evaluateDerivative(activityInh[t][n]))/(numberOfNeurons - 1.0) + (responseInh->evaluateDerivative(activityInh[t][n]))/(numberOfNeurons - 1.0) ) < 0.0)&&
                (activityInh[t][n] < responseInh->getMidPoint()) )
			{
				numberSilent++;
			}
			else if( ((-1.0 - (responseExc->evaluateDerivative(activityInh[t][n]))/(numberOfNeurons - 1.0)  + (responseInh->evaluateDerivative(activityInh[t][n]))/(numberOfNeurons - 1.0) ) < 0.0)&&
                     (activityInh[t][n] > responseInh->getMidPoint()) )
			{
                numberLoud++;
            }

			//if(activityInh[t][n] < responseInh.getThreshold())
			//{numberSilent++;}
			//else if(activityInh[t][n] > responseInh.getSaturation())
			//{numberLoud++;}

			else{numberTransition++;}
		}

		if((numberTransition == 0)&&(numberLoud == 0))
		{trajectoryStates[t] = TS_SILENT;}
		else if((numberTransition == 0)&&(numberSilent == 0))
		{trajectoryStates[t] = TS_LOUD;}
		else if(numberTransition == 0)
		{trajectoryStates[t] = TS_KREGION;}
		else if(numberTransition == 1)
		{trajectoryStates[t] = TS_TREGION;}
		else
		{trajectoryStates[t] = TS_DEFAULT;}
	}
}

void NeuroSim::updateWeights(float* image)
{
    int increaseWeightsInh;
	float learningRate = 0.0001;
    float mE = 0.0, mI = 1.0;

	for(int i = 0; i < numberOfNeurons; ++i)
	{
        if(inputExc[0][i] > image[i])
        {
            increaseWeightsInh = 1;
        }
        else
        {
            increaseWeightsInh = -1;
        }

		for(int j = 0; j < numberOfNeurons; ++j)
		{
			if(activityInh[0][j] > 0.5)
			{
				weightEI[i][j] += increaseWeightsInh*learningRate;
			}

            if(weightEI[i][j] < 0.0){weightEI[i][j] = 0.0;}
            if(weightEE[i][j] < 0.0){weightEE[i][j] = 0.0;}
			if(weightIE[i][j] < 0.0){weightIE[i][j] = 0.0;}
            if(weightII[i][j] < 0.0){weightII[i][j] = 0.0;}
		}
	}
}

void NeuroSim::somaticFilterModelRK4Step()
{
	float uEE,uII,uEI,uIE;

	for(int trajectory = 0; trajectory < numberOfTrajectories; ++trajectory)
	{
		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uII = 0.0;
            uEI = 0.0;
            uIE = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*activityExc[trajectory][preneuron];
                uEI += weightEI[neuron][preneuron]*activityInh[trajectory][preneuron];
                uIE += weightIE[neuron][preneuron]*activityExc[trajectory][preneuron];
				uII += weightEI[neuron][preneuron]*activityInh[trajectory][preneuron];
			}

			k1Exc[neuron] = (-activityExc[trajectory][neuron] + responseExc->evaluate(uEE + externalDrive[trajectory][neuron]) - responseInh->evaluate(uEI))/tauExc;
            k1Inh[neuron] = (-activityInh[trajectory][neuron] + responseExc->evaluate(uIE) - responseInh->evaluate(uII))/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uII = 0.0;
            uEI = 0.0;
            uIE = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*(activityExc[trajectory][preneuron] + 0.5*timeStep*k1Exc[preneuron]);
				uEI += weightEI[neuron][preneuron]*(activityInh[trajectory][preneuron] + 0.5*timeStep*k1Inh[preneuron]);
                uIE += weightIE[neuron][preneuron]*(activityExc[trajectory][preneuron] + 0.5*timeStep*k1Exc[preneuron]);
				uII += weightII[neuron][preneuron]*(activityInh[trajectory][preneuron] + 0.5*timeStep*k1Inh[preneuron]);
			}

			k2Exc[neuron] = (-(activityExc[trajectory][neuron] + 0.5*timeStep*k1Exc[neuron]) + responseExc->evaluate(uEE + externalDrive[trajectory][neuron]) - responseInh->evaluate(uEI))/tauExc;
			k2Inh[neuron] = (-(activityInh[trajectory][neuron] + 0.5*timeStep*k1Inh[neuron]) + responseExc->evaluate(uIE) - responseInh->evaluate(uII))/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uII = 0.0;
            uEI = 0.0;
            uIE = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*(activityExc[trajectory][preneuron] + 0.5*timeStep*k2Exc[preneuron]);
				uEI += weightEI[neuron][preneuron]*(activityInh[trajectory][preneuron] + 0.5*timeStep*k2Inh[preneuron]);
                uIE += weightIE[neuron][preneuron]*(activityExc[trajectory][preneuron] + 0.5*timeStep*k2Exc[preneuron]);
				uII += weightII[neuron][preneuron]*(activityInh[trajectory][preneuron] + 0.5*timeStep*k2Inh[preneuron]);
			}

			k3Exc[neuron] = (-(activityExc[trajectory][neuron] + 0.5*timeStep*k2Exc[neuron]) + responseExc->evaluate(uEE + externalDrive[trajectory][neuron]) - responseInh->evaluate(uEI))/tauExc;
			k3Inh[neuron] = (-(activityInh[trajectory][neuron] + 0.5*timeStep*k2Inh[neuron]) + responseExc->evaluate(uIE) - responseInh->evaluate(uII))/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uII = 0.0;
            uEI = 0.0;
            uIE = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*(activityExc[trajectory][preneuron] + timeStep*k3Exc[preneuron]);
				uEI += weightEI[neuron][preneuron]*(activityInh[trajectory][preneuron] + timeStep*k3Inh[preneuron]);
                uIE += weightIE[neuron][preneuron]*(activityExc[trajectory][preneuron] + timeStep*k3Exc[preneuron]);
				uII += weightII[neuron][preneuron]*(activityInh[trajectory][preneuron] + timeStep*k3Inh[preneuron]);
			}

			k4Exc[neuron] = (-(activityExc[trajectory][neuron] + timeStep*k3Exc[neuron]) + responseExc->evaluate(uEE + externalDrive[trajectory][neuron]) - responseInh->evaluate(uEI))/tauExc;
			k4Inh[neuron] = (-(activityInh[trajectory][neuron] + timeStep*k3Inh[neuron]) + responseExc->evaluate(uIE) - responseInh->evaluate(uII))/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			activityExc[trajectory][neuron] += (timeStep/6.0)*(k1Exc[neuron] + 2.0*k2Exc[neuron] + 2.0*k3Exc[neuron] + k4Exc[neuron]);
			if(activityExc[trajectory][neuron] < 0.0){activityExc[trajectory][neuron] = 0.0;}
			activityInh[trajectory][neuron] += (timeStep/6.0)*(k1Inh[neuron] + 2.0*k2Inh[neuron] + 2.0*k3Inh[neuron] + k4Inh[neuron]);
			if(activityInh[trajectory][neuron] < 0.0){activityInh[trajectory][neuron] = 0.0;}
		}
	}
}

void NeuroSim::synapticFilterModelRK4Step()
{
	float uEE,uEI,uIE,uII;

	for(int trajectory = 0; trajectory < numberOfTrajectories; ++trajectory)
	{
		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uEI = 0.0;
            uIE = 0.0;
            uII = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{

                uEE += weightEE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron]);
				uEI += weightEI[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron]);
                uIE += weightIE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron]);
				uII += weightII[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron]);
			}
            uEE += externalDrive[trajectory][neuron];
			uIE += 0.01;

			inputExc[trajectory][neuron] = uEE - uEI;
			inputInh[trajectory][neuron] = uIE - uII;

			k1Exc[neuron] = (-activityExc[trajectory][neuron] + uEE - uEI)/tauExc;
			k1Inh[neuron] = (-activityInh[trajectory][neuron] + uIE - uII)/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uEI = 0.0;
            uIE = 0.0;
            uII = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron] + 0.5*timeStep*k1Exc[preneuron]);
				uEI += weightEI[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron] + 0.5*timeStep*k1Inh[preneuron]);
                uIE += weightIE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron] + 0.5*timeStep*k1Exc[preneuron]);
				uII += weightII[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron] + 0.5*timeStep*k1Inh[preneuron]);
			}
            uEE += externalDrive[trajectory][neuron];
			uIE += 0.01;

			k2Exc[neuron] = (-(activityExc[trajectory][neuron] + 0.5*timeStep*k1Exc[neuron]) + uEE - uEI)/tauExc;
			k2Inh[neuron] = (-(activityInh[trajectory][neuron] + 0.5*timeStep*k1Inh[neuron]) + uIE - uII)/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uEI = 0.0;
            uIE = 0.0;
            uII = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron] + 0.5*timeStep*k2Exc[preneuron]);
				uEI += weightEI[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron] + 0.5*timeStep*k2Inh[preneuron]);
                uIE += weightIE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron] + 0.5*timeStep*k2Exc[preneuron]);
				uII += weightII[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron] + 0.5*timeStep*k2Inh[preneuron]);
			}
            uEE += externalDrive[trajectory][neuron];
			uIE += 0.01;

			k3Exc[neuron] = (-(activityExc[trajectory][neuron] + 0.5*timeStep*k2Exc[neuron]) + uEE - uEI)/tauExc;
			k3Inh[neuron] = (-(activityInh[trajectory][neuron] + 0.5*timeStep*k2Inh[neuron]) + uIE - uII)/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			uEE = 0.0;
			uEI = 0.0;
            uIE = 0.0;
            uII = 0.0;
			for(int preneuron = 0; preneuron < numberOfNeurons; ++preneuron)
			{
				uEE += weightEE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron] + timeStep*k3Exc[preneuron]);
				uEI += weightEI[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron] + timeStep*k3Inh[preneuron]);
                uIE += weightIE[neuron][preneuron]*responseExc->evaluate(activityExc[trajectory][preneuron] + timeStep*k3Exc[preneuron]);
				uII += weightII[neuron][preneuron]*responseInh->evaluate(activityInh[trajectory][preneuron] + timeStep*k3Inh[preneuron]);
			}
            uEE += externalDrive[trajectory][neuron];
			uIE += 0.01;

			k4Exc[neuron] = (-(activityExc[trajectory][neuron] + timeStep*k3Exc[neuron]) + uEE - uEI)/tauExc;
			k4Inh[neuron] = (-(activityInh[trajectory][neuron] + timeStep*k3Inh[neuron]) + uIE - uII)/tauInh;
		}

		for(int neuron = 0; neuron < numberOfNeurons; ++neuron)
		{
			activityExc[trajectory][neuron] += (timeStep/6.0)*(k1Exc[neuron] + 2.0*k2Exc[neuron] + 2.0*k3Exc[neuron] + k4Exc[neuron]);
			activityExc[trajectory][neuron] += 0.00001*(2.0*random->doub()-1.0);
			if(activityExc[trajectory][neuron] < 0.0){activityExc[trajectory][neuron] = 0.0;}
			activityInh[trajectory][neuron] += (timeStep/6.0)*(k1Inh[neuron] + 2.0*k2Inh[neuron] + 2.0*k3Inh[neuron] + k4Inh[neuron]);
			activityInh[trajectory][neuron] += 0.00001*(2.0*random->doub()-1.0);
			if(activityInh[trajectory][neuron] < 0.0){activityInh[trajectory][neuron] = 0.0;}
		}
	}
}

void NeuroSim::update(int numberOfIterations)
{
	float maxWEE = 0,maxWEI = 0,maxWIE = 0,maxWII = 0;

	for(int iterator = 0; iterator < numberOfIterations; ++iterator)
	{
        synapticFilterModelRK4Step();
		//updateWeights();
		updateCounter++;
	}

	classifyTrajectoryStates();
}

void NeuroSim::release()
{
	for(int i = 0; i < numberOfNeurons; ++i)
	{
		delete [] weightEE[i];
		delete [] weightEI[i];
	}

	delete [] weightEE;
	delete [] weightEI;

	for(int i = 0; i < numberOfTrajectories; ++i)
	{
		delete [] activityExc[i];
		delete [] activityInh[i];
	}

	delete [] activityExc;
	delete [] activityInh;

	numberOfNeurons = 0;
	numberOfTrajectories = 0;
}